package Controller;

import Dao.EmpresasJpaController;
import Dao.exceptions.NonexistentEntityException;
import Entity.Empresas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "EmpresasController", urlPatterns = {"/EmpresasController"})
public class EmpresasController extends HttpServlet 
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EmpresasController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EmpresasController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        
        //Generar Lista
        EmpresasJpaController dao = new EmpresasJpaController();
        List<Empresas> empresa = dao.findEmpresasEntities();
        request.setAttribute("AtrEmpresas", empresa);

        //Redirecciona al jsp
        request.getRequestDispatcher("listadoEmpresas.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        
        //String accion = request.getParameter("btnAccion");
        String btnNuevo = null;
        String btnEditar = null;
        String btnEliminar = null;
        String rutEmpresa = null;
        String btnAccion = null;
        
        String txtRutEmpresa = "";
        String txtRazonSocial = "";
        String txtRubro = "";
        String txtEmail = "";
        String txtTelefono = "";        

        try {btnNuevo = request.getParameter("btnNuevo");} catch (Exception e) {}
        try {btnEditar = request.getParameter("btnEditar");} catch (Exception e) {}
        try {btnEliminar = request.getParameter("btnEliminar");} catch (Exception e) {}
        try {btnAccion = request.getParameter("btnAccion");} catch (Exception e) {}
        
        EmpresasJpaController dao = new EmpresasJpaController();
        
        if (btnNuevo != null)
        {
            request.getRequestDispatcher("nuevaEmpresa.jsp").forward(request, response);
        }
        if (btnEditar != null)
        {
            rutEmpresa = btnEditar;
            Empresas bEmp = dao.findEmpresas(rutEmpresa);
            
            request.setAttribute("infoEmpresa", bEmp);            
            request.getRequestDispatcher("editarEmpresa.jsp").forward(request, response);
        }
        if (btnEliminar != null)
        {
            rutEmpresa = btnEliminar;
            
            try {
                dao.destroy(rutEmpresa);
                
                //Generar Lista
                List<Empresas> emp = dao.findEmpresasEntities();
                request.setAttribute("AtrEmpresas", emp);

                //Redirecciona al jsp
                request.getRequestDispatcher("listadoEmpresas.jsp").forward(request, response);                 
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(EmpresasController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        if (btnAccion != null)
        {
            if (btnAccion.equals("Guardar"))
            {
                txtRutEmpresa = request.getParameter("txtRutEmpresa").toUpperCase();
                txtRazonSocial = request.getParameter("txtRazonSocial").toUpperCase();
                txtRubro = request.getParameter("txtRubro").toUpperCase();
                txtEmail = request.getParameter("txtEmail").toUpperCase();
                txtTelefono = request.getParameter("txtTelefono");
                
                Empresas e = new Empresas(txtRutEmpresa, txtRazonSocial, txtRubro, txtEmail, txtTelefono);
                try {
                    dao.create(e);
                    
                    //Generar Lista
                    List<Empresas> emp = dao.findEmpresasEntities();
                    request.setAttribute("AtrEmpresas", emp);

                    //Redirecciona al jsp
                    request.getRequestDispatcher("listadoEmpresas.jsp").forward(request, response);            
                    
                } catch (Exception ex) {
                    Logger.getLogger(EmpresasController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if (btnAccion.equals("Modificar"))
            {
                txtRutEmpresa = request.getParameter("txtRutEmpresa").toUpperCase();
                txtRazonSocial = request.getParameter("txtRazonSocial").toUpperCase();
                txtRubro = request.getParameter("txtRubro").toUpperCase();
                txtEmail = request.getParameter("txtEmail").toUpperCase();
                txtTelefono = request.getParameter("txtTelefono");
                
                Empresas mEmp = new Empresas(txtRutEmpresa,txtRazonSocial,txtRubro,txtEmail,txtTelefono);
                
                try {
                    dao.edit(mEmp);
                    
                    //Generar Lista
                    List<Empresas> emp = dao.findEmpresasEntities();
                    request.setAttribute("AtrEmpresas", emp);

                    //Redirecciona al jsp
                    request.getRequestDispatcher("listadoEmpresas.jsp").forward(request, response);                     
                } catch (Exception ex) {
                    Logger.getLogger(EmpresasController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if (btnAccion.equals("Cancelar"))
            {
                try {
                    //Generar Lista
                    List<Empresas> emp = dao.findEmpresasEntities();
                    request.setAttribute("AtrEmpresas", emp);

                    //Redirecciona al jsp
                    request.getRequestDispatcher("listadoEmpresas.jsp").forward(request, response);            
                    
                } catch (Exception ex) {
                    Logger.getLogger(EmpresasController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }        
        }
        
    }

    @Override
    public String getServletInfo() 
    {
        return "Short description";
    }

}
