package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "empresas")
@NamedQueries({
    @NamedQuery(name = "Empresas.findAll", query = "SELECT e FROM Empresas e"),
    @NamedQuery(name = "Empresas.findByRutempresa", query = "SELECT e FROM Empresas e WHERE e.rutempresa = :rutempresa"),
    @NamedQuery(name = "Empresas.findByRazonsocial", query = "SELECT e FROM Empresas e WHERE e.razonsocial = :razonsocial"),
    @NamedQuery(name = "Empresas.findByRubro", query = "SELECT e FROM Empresas e WHERE e.rubro = :rubro"),
    @NamedQuery(name = "Empresas.findByEmail", query = "SELECT e FROM Empresas e WHERE e.email = :email"),
    @NamedQuery(name = "Empresas.findByTelefono", query = "SELECT e FROM Empresas e WHERE e.telefono = :telefono")})
public class Empresas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rutempresa")
    private String rutempresa;
    @Size(max = 2147483647)
    @Column(name = "razonsocial")
    private String razonsocial;
    @Size(max = 2147483647)
    @Column(name = "rubro")
    private String rubro;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(name = "email")
    private String email;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;

    public Empresas() 
    {
    }

    public Empresas(String rutempresa, String razonsocial, String rubro, String email, String telefono) 
    {
        this.rutempresa = rutempresa;
        this.razonsocial = razonsocial;
        this.rubro = rubro;
        this.email = email;
        this.telefono = telefono;
    }

    
    
    public Empresas(String rutempresa) 
    {
        this.rutempresa = rutempresa;
    }

    public String getRutempresa() 
    {
        return rutempresa;
    }

    public void setRutempresa(String rutempresa) 
    {
        this.rutempresa = rutempresa;
    }

    public String getRazonsocial() 
    {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) 
    {
        this.razonsocial = razonsocial;
    }

    public String getRubro() 
    {
        return rubro;
    }

    public void setRubro(String rubro) 
    {
        this.rubro = rubro;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getTelefono() 
    {
        return telefono;
    }

    public void setTelefono(String telefono) 
    {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutempresa != null ? rutempresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresas)) {
            return false;
        }
        Empresas other = (Empresas) object;
        if ((this.rutempresa == null && other.rutempresa != null) || (this.rutempresa != null && !this.rutempresa.equals(other.rutempresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Empresas[ rutempresa=" + rutempresa + " ]";
    }
    
}
