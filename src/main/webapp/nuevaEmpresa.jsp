<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nueva Empresa</title>

        <%--CDN Bootstrap 5 --%>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>                
        
        <%--Javascript--%>
        <script src="js/funciones.js"></script>
    </head>
    <body>
        <jsp:include page="vwHeader.jsp"></jsp:include> 
        <br>
        <form name="newEmpresa" action="EmpresasController" method="POST">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Nueva Empresa</h3>
                        <hr>
                    </div>
                    <div class="col-sm-5">
                        <div class="mb-2">
                          <label for="txtEmail" class="form-label">Rut Empresa</label>
                          <input type="text" class="form-control" id="txtRutEmpresa" name="txtRutEmpresa" placeholder="Ej. 76123456-7" autocomplete="off" required="" onkeypress="return DigitosRut(event);" oninput="checkRut(this)" maxlength="10" style="text-transform:uppercase">
                        </div>
                        <div class="mb-2">
                          <label for="txtEmail" class="form-label">Razón Social</label>
                          <input type="text" class="form-control" id="txtRazonSocial" name="txtRazonSocial" placeholder="Ej. Empresa de transporte S.A." autocomplete="off" required="" style="text-transform:uppercase">
                        </div>
                        <div class="mb-2">
                          <label for="txtEmail" class="form-label">Rubro</label>
                          <input type="text" class="form-control" id="txtRubro" name="txtRubro" placeholder="Ej. Transporte Público" autocomplete="off" required="" style="text-transform:uppercase">
                        </div>                         
                        <div class="mb-2">
                          <label for="txtEmail" class="form-label">Email</label>
                          <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Ej. nombre@empresa.cl" autocomplete="off" required="" style="text-transform:uppercase">
                        </div>
                        <div class="mb-2">
                          <label for="txtEmail" class="form-label">Teléfono</label>
                          <input type="text" class="form-control" id="txtTelefono" name="txtTelefono" placeholder="Ej. 56912345678" autocomplete="off" required="" maxlength="11" onkeypress="return SoloNumeros(event);">
                        </div> 
                    </div>    
                    <div class="col-sm-12">                        
                        <hr>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" id="btnGuardar" name="btnAccion" value="Guardar" class="btn btn-primary">Guardar</button>
                        <button type="submit" id="btnCancelar" name="btnAccion" value="Cancelar" class="btn btn-primary" formnovalidate="">Cancelar</button>
                    </div>
                </div>
            </div>
        </form>
    </body>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>    
</html>
