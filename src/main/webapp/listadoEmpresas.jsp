<%@page import="java.util.Iterator"%>
<%@page import="Entity.Empresas"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //Lista de empresas
    List<Empresas> listaEmpresas = (List<Empresas>)request.getAttribute("AtrEmpresas");
    Iterator<Empresas> iteratorEmpresas = listaEmpresas.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado de Empresas</title>

        <%--CDN Bootstrap 5 --%>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>        
    </head>
    <body>
        <%@include file="vwHeader.jsp" %>
        <br>
        <form name="vwEmpresas" action="EmpresasController" method="POST">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12">
                    <h3>Listado de Empresas</h3>   
                    <button type="submit" id="btnNuevo" name="btnNuevo" value="C" class="btn btn-primary btn-sm">Nueva Empresa</button>                    
                    <hr>
                    <table border="0" class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Rut Empresa</th>
                                <th scope="col">Razón Social</th>
                                <th scope="col">Rubro</th>
                                <th scope="col">Email</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  while (iteratorEmpresas.hasNext())
                                {
                                    Empresas e = iteratorEmpresas.next();%>                                
                                    <tr>
                                        <td scope="row"><%= e.getRutempresa() %></td>
                                        <td><%= e.getRazonsocial() %></td>
                                        <td><%= e.getRubro() %></td>
                                        <td><%= e.getEmail() %></td>
                                        <td><%= e.getTelefono() %></td>
                                        <td>
                                            <button id="btnEditar" name="btnEditar" class="btn btn-primary btn-sm" style="width: 100px;" value="<%= e.getRutempresa() %>">Editar</button>
                                            <button id="btnEliminar" name="btnEliminar" class="btn btn-primary btn-sm" style="width: 100px;" value="<%= e.getRutempresa() %>" onclick="return(confirm('¿Eliminar Empresa <%= e.getRazonsocial() %>?'))">Eliminar</button>
                                        </td>
                                    </tr>
                            <%  }%>                            
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
        </form>
    </body>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>    
</html>
